package com.schibsted.radek_krzych;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CacheTest {
	
	private static final Logger logger = LoggerFactory.getLogger(CacheTest.class);
	
	@Test
	public void addingToCacheTest(){
		logger.debug("test 1");
		Cache<String> cache = new Cache<String>(60);
		try {
			cache.put("jeden", "jeden");
			cache.put("jeden", "dwa");
			cache.put("trzy", "trzy");
			cache.put("cztery", "cztery");
		} catch (Exception e) {
			logger.debug("exception: "+e.getMessage());
			logger.debug("exception: "+e.getMessage());
		}
		
		assertNotNull(cache);
		assertEquals(3, cache.getSize());
	}
	
	@Test
	public void refreshCacheRemoveAllTest(){
		logger.debug("test 2");
		Cache<String> cache = new Cache<String>(5);
		try {
			cache.put("jeden", "jeden");
			cache.put("trzy", "trzy");
			cache.put("cztery", "cztery");
		} catch (Exception e) {
			logger.debug("exception: "+e.getMessage());
		}
		
		assertNotNull(cache);
		assertEquals(3, cache.getSize());
		
		try {
			Thread.sleep(8 * 1000);
		} catch (InterruptedException e) {
			logger.debug("exception: "+e.getMessage());
		}

		cache.refresh();
		assertEquals(0, cache.getSize());
	}

	@Test
	public void refreshCacheLeaveAllTest(){
		logger.debug("test 3");
		Cache<Integer> cache = new Cache<Integer>(60);
		try {
			cache.put("jeden", new Integer("1"));
			cache.put("trzy", new Integer("3"));
			cache.put("cztery", new Integer("4"));
		} catch (Exception e) {
			logger.debug("exception: "+e.getMessage());
		}
		
		assertNotNull(cache);
		assertEquals(3, cache.getSize());
		
		try {
			Thread.sleep(1 * 1000);
		} catch (InterruptedException e) {
			logger.debug("exception: "+e.getMessage());
		}

		cache.refresh();
		assertEquals(3, cache.getSize());
	}

	
	@Test
	public void refreshCacheLeaveHalfTest(){
		logger.debug("test 4");
		Cache<Double> cache = new Cache<Double>(5);
		try {
			cache.put("jedenaście", new Double(11));
			cache.put("dwanaście", new Double(12));
		} catch (Exception e) {
			logger.debug("exception: "+e.getMessage());
		}
		
		assertNotNull(cache);
		assertEquals(2, cache.getSize());
		
		try {
			Thread.sleep(8 * 1000);
		} catch (InterruptedException e) {
			logger.debug("exception: "+e.getMessage());
		}
		
		try {
			cache.put("dwadzieścia jeden", new Double(21));
			cache.put("dwadzieścia dwa", new Double(22));
		} catch (Exception e) {
			logger.debug("exception: "+e.getMessage());
		}
		
		assertNotNull(cache);
		assertEquals(4, cache.getSize());

		cache.refresh();
		assertEquals(2, cache.getSize());
	}
	
	@Test
	public void getAllNoValidObjTest(){
		logger.debug("test 5");
		Cache<String> cache = new Cache<String>(5);
		try {
			cache.put("jeden", "jeden");
			cache.put("trzy", "trzy");
			cache.put("cztery", "cztery");
		} catch (Exception e) {
			logger.debug("exception: "+e.getMessage());
		}
		
		assertNotNull(cache);
		assertEquals(3, cache.getSize());
		
		try {
			Thread.sleep(8 * 1000);
		} catch (InterruptedException e) {
			logger.debug("exception: "+e.getMessage());
		}

		List<String> allObjects = cache.getAll();
		assertEquals(0, allObjects.size());
	}
	
	@Test
	public void getAllAllValidObjTest(){
		logger.debug("test 6");
		Cache<Integer> cache = new Cache<Integer>(60);
		try {
			cache.put("jeden", new Integer("1"));
			cache.put("trzy", new Integer("3"));
			cache.put("cztery", new Integer("4"));
		} catch (Exception e) {
			logger.debug("exception: "+e.getMessage());
		}
		
		assertNotNull(cache);
		assertEquals(3, cache.getSize());
		
		try {
			Thread.sleep(1 * 1000);
		} catch (InterruptedException e) {
			logger.debug("exception: "+e.getMessage());
		}

		List<Integer> allObjects = cache.getAll();
		assertEquals(3, allObjects.size());
	}
	
	@Test
	public void getAllgetHalfObjTest(){
		logger.debug("test 7");
		Cache<Double> cache = new Cache<Double>(5);
		try {
			cache.put("jedenaście", new Double(11));
			cache.put("dwanaście", new Double(12));
		} catch (Exception e) {
			logger.debug("exception: "+e.getMessage());
		}
		
		assertNotNull(cache);
		assertEquals(2, cache.getSize());
		
		try {
			Thread.sleep(8 * 1000);
		} catch (InterruptedException e) {
			logger.debug("exception: "+e.getMessage());
		}
		
		try {
			cache.put("dwadzieścia jeden", new Double(21));
			cache.put("dwadzieścia dwa", new Double(22));
		} catch (Exception e) {
			logger.debug("exception: "+e.getMessage());
		}
		
		assertNotNull(cache);
		assertEquals(4, cache.getSize());

		List<Double> allObjects = cache.getAll();
		assertEquals(2, allObjects.size());

	}
	
	@Test
	public void getSizeTest(){
		logger.debug("test 8");
		Cache<Double> cache = new Cache<Double>(50);
		try {
			cache.put("jedenaście", new Double(11));
			cache.put("dwanaście", new Double(12));
			cache.put("dwadzieścia jeden", new Double(21));
			cache.put("dwadzieścia dwa", new Double(22));
		} catch (Exception e) {
			logger.debug("exception: "+e.getMessage());
		}
		
		assertEquals(4, cache.getSize());
		
	}
	

	
}
