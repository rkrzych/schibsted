package com.schibsted.radek_krzych;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.Oneway;

public class Cache<T> {

	int interval;
	Map<String, CachedObject<T>> objects;

	public Cache(int interval) {
		this.interval = interval;
		this.objects = new HashMap<String, CachedObject<T>>();
	}

	public void put(String key, T object) throws Exception {
		Utils.checkIfNotNull("key", key);
		Utils.checkIfNotNull("object", object);

		CachedObject<T> cachedObject = new CachedObject<T>(object);
		this.objects.put(key, cachedObject);

	}

	public T getValue(String key) {
		CachedObject<T> obj = objects.get(key);
		if (obj != null && obj.isValid(this.interval)) {
			return obj.get();
		} else {
			objects.remove(key);
		}

		return null;
	}

	public List<T> getAll() {
		List<T> objList = new ArrayList<>();
		List<String> toDelete = new ArrayList<>();

		for (String key : objects.keySet()) {
			CachedObject<T> cO = objects.get(key);
			if (cO.isValid(interval)) {
				objList.add(cO.get());
			} else {
				toDelete.add(key);
			}
		}

		clear(toDelete);
		return objList;
	}

	public void refresh() {
		List<String> toDelete = new ArrayList<>();
		for (String key : objects.keySet()) {
			CachedObject<T> cO = objects.get(key);
			if (!cO.isValid(interval)) {
				toDelete.add(key);
			}
		}
		clear(toDelete);
	}
	
	public int getSize(){
		return objects.size();
	}

	public void changeInterval(int interval) {
		this.interval = interval;
	}

	private void clear(List<String> toDelete) {
		for (String key : toDelete) {
			objects.remove(key);
		}
	}

}
