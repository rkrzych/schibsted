package com.schibsted.radek_krzych;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

	private static final Logger logger = LoggerFactory.getLogger(Main.class);
	
	public static void main(String[] args) {
		logger.debug("start");
		Cache<String> cache = new Cache<String>(30);
		try {
			cache.put("jeden", "jeden");
			cache.put("jeden", "dwa");
			cache.put("trzy", "trzy");
			cache.put("cztery", "cztery");
		} catch (Exception e) {
			logger.debug(e.getMessage());
		}

		try {
			Thread.sleep(60 * 1000);
		} catch (InterruptedException e) {
			logger.debug("exception: "+e.getMessage());
		}

		List<String> allObjects = cache.getAll();

		logger.debug("Cache size: " + allObjects.size());

		for (String obj : allObjects) {
			logger.debug("object: " + obj);
		}

		logger.debug("stop");

	}

}
