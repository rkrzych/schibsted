package com.schibsted.radek_krzych;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

public class CachedObject<T> {

	Date addedDate;
	T object;

	public CachedObject(T object) {
		this.addedDate = new Date();
		this.object = object;
	}

	public T get() {
		return object;
	}

	public boolean isValid(int interval) {
		Date now = new Date();
		Date rollbackDate = DateUtils.addSeconds(addedDate, interval);
		if (rollbackDate.before(now)) {
			return false;
		} else if (rollbackDate.equals(now) || rollbackDate.after(now)) {
			return true;
		}
		return false;

	}

}
